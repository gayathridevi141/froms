import { Form, Button } from "react-bootstrap"

import {EmployeeContext} from './contexts/FromContext';
import {useContext, useState} from 'react';

const EditForm = ({theEmployee}) =>{

    const id = theEmployee.id;
    const [salutation, setSalutation] = useState(theEmployee.firstname);
    const [firstname, setFirstName] = useState(theEmployee.firstname);
    const [lastname, setLastName] = useState(theEmployee.firstname);
    const [email, setEmail] = useState(theEmployee.email);
    const [date, setDate] = useState(theEmployee.date);
    const [address, setAddress] = useState(theEmployee.address);
    


    const {updateEmployee} = useContext(EmployeeContext);

    const updatedEmployee = {id, salutation, firstname, lastname, email, date, address}

    const handleSubmit = (e) => {
        e.preventDefault();
        updateEmployee(id, updatedEmployee)
    }

     return (

        <Form onSubmit={handleSubmit}>

            
<Form.Group>
                    <label>Salutation:</label>
                <select name="salutation" value={salutation}  onChange={(e)=> setSalutation(e.target.value)}>
                    <option value={salutation}  onChange={(e)=> setSalutation(e.target.value)}>None</option>
                    <option value={salutation}  onChange={(e)=> setSalutation(e.target.value)}>eg..</option>
                    <option value={salutation} onChange={(e)=> setSalutation(e.target.value)}>eg2..</option>
                    <option value={salutation}  onChange={(e)=> setSalutation(e.target.value)}>eg3..</option>
                </select>
            </Form.Group>
              <Form.Group>
                <Form.Control
                    type="text"
                    placeholder="Name *"
                    name="name"
                    value={firstname}
                    onChange={(e)=> setFirstName(e.target.value)}
                    required
                />
            </Form.Group>
            <Form.Group>
                <Form.Control
                    type="text"
                    placeholder="Name *"
                    name="name"
                    value={firstname}
                    onChange={(e)=> setFirstName(e.target.value)}
                    required
                />
            </Form.Group>
            <Form.Group>
                <Form.Control
                    type="text"
                    placeholder="Name *"
                    name="name"
                    value={firstname}
                    onChange={(e)=> setLastName(e.target.value)}
                    required
                />
            </Form.Group>
            <Form.Group>
                <Form.Control
                    type="email"
                    placeholder="Email *"
                    name="email"
                    value={email}
                    onChange={(e)=> setEmail(e.target.value)}
                    required
                />
            </Form.Group>
            <Form.Group>
                <Form.Control
                    as="textarea"
                    placeholder="Address"
                    rows={3}
                    name="address"
                    value={address}
                    onChange={(e)=> setAddress(e.target.value)}
                />
            </Form.Group>
            <Form.Group>
                <Form.Control
                    type="date"
                    placeholder="Date of Birth"
                    name="date"
                    value={date}
                    onChange={(e)=> setDate(e.target.value)}
                />
            </Form.Group>
            <Button variant="success" type="submit" block>
                Edit From
            </Button>

        </Form>

     )
}

export default EditForm;