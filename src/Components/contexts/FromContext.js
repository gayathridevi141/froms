import {createContext, useEffect, useState} from 'react';
import { v4 as uuidv4 } from 'uuid';

export const EmployeeContext = createContext()

const EmployeeContextProvider  = (props) => {

    const [employees, setEmployees] = useState([
        {id:uuidv4(), salutation:'None', firstname: 'Nisha10', lastname: 'nisha' ,email: 'nisha@mail.com', date: 23-5-2000 , address: '89 Chiaroscuro'},
        {id:uuidv4(), salutation:'eg' , firstname: 'Asha', lastname:'H s' , email: 'nila@mail.com', date: 22-7-2000, address: '57, Berlin'},
       
])

useEffect(()=> {
    setEmployees(JSON.parse(localStorage.getItem('employees')))
},[])

useEffect(() => {
    localStorage.setItem('FromDate', JSON.stringify(employees));
})



const sortedEmployees = employees.sort((a,b)=>(a.name < b.name ? -1 : 1));



const addEmployee = ( salutation, firstname, lastname, email, date ,address) => {
    setEmployees([...employees , {id:uuidv4(),salutation, firstname, lastname, email, date, address}])
}

const deleteEmployee = (id) => {
    setEmployees(employees.filter(employee => employee.id !== id))
}

const updateEmployee = (id, updatedEmployee) => {
    setEmployees(employees.map((employee) => employee.id === id ? updatedEmployee : employee))
}

    return (
        <EmployeeContext.Provider value={{sortedEmployees, addEmployee, deleteEmployee, updateEmployee}}>
            {props.children}
        </EmployeeContext.Provider>
    )
}

export default EmployeeContextProvider;